import {Injectable} from '@angular/core';
import {ToDoEntry} from './ToDoEntry';
import {AddTodoComponent} from './add-todo/add-todo.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public todoList: ToDoEntry[];

  constructor(private modalService: NgbModal, private http: HttpClient) {
    this.http.get('http://localhost:8080/todolist').toPromise().then((res: any) => {
      this.todoList = res.todoList;
    });
  }

  public done(index: number): void {
    this.http.put('http://localhost:8080/done/' + index, {}).toPromise().then((res: any) => {
    });
    this.http.get('http://localhost:8080/todolist').toPromise().then((res: any) => {
      this.todoList = res.todoList;
    });
  }

  public undo(index: number): void {
    this.http.put('http://localhost:8080/undone/' + index, {}).toPromise().then((res: any) => {
    });
    this.http.get('http://localhost:8080/todolist').toPromise().then((res: any) => {
      this.todoList = res.todoList;
    });
  }

  public destroy(index: number): void {
    this.http.delete('http://localhost:8080/entry/' + index).toPromise().then((res: any) => {
    });
    this.http.get('http://localhost:8080/todolist').toPromise().then((res: any) => {
      this.todoList = res.todoList;
    });
  }

  async add(): Promise<void> {
    const modal = this.modalService.open(AddTodoComponent);
    try {
      const title = await modal.result;
      this.http.post('http://localhost:8080/entry', {title}).toPromise().then((res: any) => {
        const message = res.message;
      });
      this.http.get('http://localhost:8080/todolist').toPromise().then((res: any) => {
        this.todoList = res.todoList;
      });
    } catch (e) {
      console.log('Modal closed', e);
    }
  }
}
