import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dateformatter'
})
export class DateformatterPipe implements PipeTransform {
  transform(value: Date, args?: any): string {
    const d = new Date(value);
    const currdate = d.getDate();
    const currmonth = d.getMonth();
    const curryear = d.getFullYear();
    const months = ['Januar', 'Februar', 'März',
      'April', 'Mai', 'Juni', 'Juli', 'August', 'September',
      'Oktober', 'November', 'Dezember'];

    const today = currdate + '-' + months[currmonth] + '-' + curryear ;
    return today;
  }

}
