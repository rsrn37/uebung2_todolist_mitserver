import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ToDoEntry} from '../ToDoEntry';

@Component({
  selector: 'app-todo-item',
  templateUrl: './todo-item.component.html',
  styleUrls: ['./todo-item.component.css']
})
export class TodoItemComponent implements OnInit {

  @Input() todo: ToDoEntry;
  @Input() bol: boolean;
  @Output() destroy: EventEmitter<void> = new EventEmitter<void>();
  @Output() done: EventEmitter<void> = new EventEmitter<void>();
  @Output() undo: EventEmitter<void> = new EventEmitter<void>();
  constructor() { }

  ngOnInit(): void {
  }

}
